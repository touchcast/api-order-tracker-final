// Check if there is a valid id
// if not display id input box
// else carry on

//function to log messages
var log = function(message) {
    console.log(message);
};

//Getting order ID from the URL
var getOrderIdParam = function(identifier) {

    var url = window.location.search.substring(1);
    var params = url.split('&');

    for (var i = 0; i < params.length; i++) {
        var param = params[i].split('=');
        if (param[0] == identifier) {
            return param[1];
        }
    }
};

//Function to VALIDATE the input box
var validateOrderIdInput = function(e) {
    //console.log("validateOrderIdInput ", validateOrderIdInput);
    var allowedChars = /[A-Za-z0-9]/g;
    var key = String.fromCharCode(e.which);
    if (e.keyCode == 8
        || e.keyCode == 37
        || e.keyCode == 39
        || allowedChars.test(key)) {
        return true;
    }
    return false;
};

//Time OUT
var displayInvalidOrderIdMessage = function(message) {
    $('.not_found_div').text(message).show().delay(3000).fadeOut();
};

//Validating the order ID's LENGTH
var orderIdInputIsValid = function(id) {
    var message = '';
    if (id.length < 1) {
        message = 'Please enter a valid number';
        displayInvalidOrderIdMessage(message);
        return false;
    }
    else if (id.length < 5) {
        message = 'Please enter minimum of 5 characters';
        displayInvalidOrderIdMessage(message);
        return false;
    }
    return true;
};

//Updated the URL with the entered Order ID
var updateOrderId = function(e) {
    var id = $('#js-order-id').val();
 
    if ( ! orderIdInputIsValid(id)) {
        return;
    }

    // update url with order id
    hideOrderInput();
    showOrder(id);
    fetchOrderStatus(id);
};
//Submit button function
var addOrderInputListeners = function() {
    console.log("inside addOrderInputListeners");
    $("#js-order-id").on("keypress", validateOrderIdInput);
    $('.js-order-id-submit').click(updateOrderId);
};

//hiding input box after submitting
var hideOrderInput = function() {
    $('.js-enter_order_number').hide();
    $("#js-order-id").off("keypress", validateOrderIdInput);
};
//show input if ID is not entered
var displayOrderInput = function() {
    //console.log($('.enter_order_number'));
    $('.js-enter_order_number').removeClass('is-hidden');
    $('.initial_welcome').hide();
    $('.initial_welcome').text("Please enter your order number below").show();
    //console.log("should show input field");
    addOrderInputListeners();
};

//showing order ID
var showOrder = function(id) {
    $('.displayOrderID').text("Reference Number: " + id);
    $('.order_content').show();
};
//Error Messages
var responseOrderNotFound = function() {
    $(".not_found_div").text("Order not found").show();
};

//changing the stage names to small letter and remove any unwanted spaces  
var stageNameToClass = function (stageName) {

    if( ! stageName) {
        return '';
    }
    return stageName.toString().toLowerCase()
        .replace(/[^a-z0-9\s]/gi, '')
        .replace(/[_\s]/g, '-');
};

//Sanitize the Order History array
var sanitizeOrderHistory = function (collection) {
    var filtered = {},
    className = '',
        total = collection.length;

    for (var i = 0; i < total; i++) {

        if (!collection[i].stage) {

            continue;
        }

        className = stageNameToClass(collection[i].stage);
        filtered[className] = {
            date: collection[i].date,
            className: stageNameToClass(collection[i].stage),
            stage: collection[i].stage
        };
    }
    return filtered;
};

// var updateOrderStatus = function (orderItem, className) {
//     if (!orderItem) {
//         return;
//     }
// };

//function to DISPLAY "orderPurpose", "customer firstName" and "orderStatus"
var updateServiceView = function(data) {

    var purpose = data.orderPurpose,
        name    = data.customerContact.firstName,
        status  = data.orderStatus;

    if (purpose) {
        $('.all_content .order_purpose').html(purpose);
    }

    if (name) {
        $('.all_content .firstname').prepend(name).show();
    }

    if (status) {

        $('.all_content .overall_status').show().append(status)
        $('.still-in-dark').css('opacity', '1');
        $('.accordion-entry').show();
        $('.fibre-journey').show();
    }
};

//Function to RE-FORMATE the date
var formatDateTime = function(datetime) {
    var monthNames = [
        "January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    var orderDate = datetime.split('-');
    datetime = orderDate[2] + '-' +
                orderDate[1] + '-' +
                orderDate[0];

    datetime = new Date(datetime);

    var day   = orderDate[0],
        month = monthNames[datetime.getMonth()],
        year  = datetime.getFullYear();

    return day +
            ' ' +
            month +
            ', ' +
            year;
};


//The following conditions are to change the BOX colors from GREY to DARK PINK or BRIGHT PINK 
//based on whatever JSON data we are receiving
var fetchOrderDone = function(data) {
    updateServiceView(data);

    if (data.orderCompletionDate) {
            datetime = data.orderCompletionDate.replace(/-/g, '/');
            // $(".fibre-journey ." + options[4].name + " .default_text").text("Estimated completion on " + datetime).show();
            $(".fibre-journey .live .default_text").text("Estimated completion on " + datetime).show();
        }

    var stage = stageNameToClass(data.currentStage);
    console.log("current stage is ", stage);

    //conditions to check "CURRENTSTAGE"    
    if (stage === "service-is-active")
    {   
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("bright-pink");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");        
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");
        

        datetime = data.orderCompletionDate.replace(/-/g, '/');
        $(".fibre-journey .live .date").text(datetime).show();
        $(".fibre-journey .live .default_text").hide();
        $(".fibre-journey .live").addClass("bright-pink");
        $(".fibre-journey .live").removeClass("grey");
        $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        
        $(".fibre-journey .installation .yellow_banner_text").text("Complete").show();  
        $(".fibre-journey .installation").addClass("dark-pink");
        $(".fibre-journey .installation").removeClass("grey");      
        $(".fibre-journey .scoping .yellow_banner_text").text("Not Required").show();
        $(".fibre-journey .consent .yellow_banner_text").text("Not Required").show();
    } 
    else if (stage === "complete") {
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .order").removeClass("bright-pink");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");        
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");

        datetime = data.orderCompletionDate.replace(/-/g, '/');
        $(".fibre-journey .live .date").text(datetime).show();
        $(".fibre-journey .live").addClass("bright-pink");
        $(".fibre-journey .live").removeClass("grey");
        $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        
        $(".fibre-journey .installation .yellow_banner_text").text("Complete").show();
        $(".fibre-journey .installation").addClass("dark-pink");
        $(".fibre-journey .installation").removeClass("grey");
        
        $(".fibre-journey .consent .yellow_banner_text").text("Consent Granted").show();
    } //END   

    else if (stage === "service-is-active" || stage === "complete") {
        $(".fibre-journey .installation").addClass("dark-pink");
        $(".fibre-journey .installation").removeClass("grey");
        $(".fibre-journey .installation .yellow_banner_text").text("Complete").show();
        
        //consent
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");
        $(".fibre-journey .consent .yellow_banner_text").text("Not Required").show();
        //scoping
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .scoping .yellow_banner_text").text("Not Required").show();
    } //END   

    //Consent           
    else if (stage === "consent-granted") {
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");

        $(".fibre-journey .consent").addClass("bright-pink");
        $(".fibre-journey .consent").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".consent .yellow_banner_text").text("Consent Granted").show();
        $(".order .yellow_banner_text").text("Order Created").show();

    } //END
    else if (stage === "awaiting-response") {
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .consent").addClass("bright-pink");
        $(".fibre-journey .consent").removeClass("grey");
        $(".consent .yellow_banner_text").text("Awaiting Response").show();

    } //END

    else if (stage === "consent-required") {
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("bright-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .consent").addClass("bright-pink");
        $(".fibre-journey .consent").removeClass("grey");
        $(".consent .yellow_banner_text").text("Consent Required").show();
        $(".order .yellow_banner_text").text("Order Created").show();
    } //END

    else if (stage === "wait-for-quote-acceptance") {
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .scoping").addClass("bright-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".scoping .yellow_banner_text").text("Wait for quote acceptance").show();
    } //END

    else if (stage === "processing") {
        $(".fibre-journey .order").addClass("bright-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".order .yellow_banner_text").text("Processing").show();
    } //END

    else if (stage === "order-created") {
        $(".fibre-journey .order").addClass("birght-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .order .date").text(data.orderDate);
        $(".order .yellow_banner_text").text("Order Created").show();

    } //END

    else if (stage === "generate-quote") {
        // alert("Order Created");
        $(".fibre-journey .scoping").addClass("bright-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".scoping .yellow_banner_text").text("Generate Quote").show();
    } //END
    else if (stage === "item-has-shipped") {
        var datetime = formatDateTime(data.orderDate);
        $(".fibre-journey .order .date").text(datetime);

        if (data.orderCompletionDate) {
            datetime = data.orderCompletionDate.replace(/-/g, '/');
            // $(".fibre-journey ." + options[4].name + " .default_text").text("Estimated completion on " + datetime).show();
            $(".fibre-journey .live .default_text").text("Estimated completion on " + datetime).show();
        }
        
    }   
    else if (data.courierTrackingNumber) {
            var track_url = "http://trackandtrace.courierpost.co.nz/Search/_courierTrackingNumber_" + data.courierTrackingNumber;
            //$(".api_order_tracker .modem_tracking").text("Your Fibre Modem has shipped on " + orderItem.date + ".").show();
            $(".track_btn").show();
            $(".track_number").text(" The tracking number is " + data.courierTrackingNumber).show();
            $('element').attr('attribute_name', 'attribute_value');
            $('a.track_link').attr('href', track_url).show();
    }
    else {
        console.log("something else");
    }


    //conditions to check "ORDERHISTORYSTAGE" array   
    var sanitizedOrderHistory = sanitizeOrderHistory(data.orderHistory);
    console.log("sanitized", sanitizedOrderHistory);

    if (sanitizedOrderHistory['service-is-active']
        && sanitizedOrderHistory['service-is-active'].className) {

            //Live
            var orderHistoryDate = formatDateTime(sanitizedOrderHistory['service-is-active'].date);
            console.log("sanitized className ", sanitizedOrderHistory['service-is-active'].className);
            console.log("sanitized date ", sanitizedOrderHistory['service-is-active'].date);

            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            $(".fibre-journey .live .default_text").text("Estimated completion on " + data.orderCompletionDate).show();
            $(".fibre-journey .live .date").text(orderHistoryDate);
            $(".fibre-journey .live .default_text").hide();

            //Installation
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");
            $(".fibre-journey .installation .yellow_banner_text").text("Complete").show();
            $(".fibre-journey .installation .date").text(orderHistoryDate);

            //Pink - All panel to the left
            $(".fibre-journey .order").addClass("dark-pink");
            $(".fibre-journey .order").removeClass("grey");
            $(".fibre-journey .scoping").addClass("dark-pink");
            $(".fibre-journey .scoping").removeClass("grey");
            $(".fibre-journey .consent").addClass("dark-pink");
            $(".fibre-journey .consent").removeClass("grey");
            $(".fibre-journey .order .date").text(orderHistoryDate);

            //order
            $(".fibre-journey .order").addClass("dark-pink");
            $(".fibre-journey .order .yellow_banner_text").text("Not Required").show();

            //scoping
            $(".fibre-journey .scoping").addClass("dark-pink");
            $(".fibre-journey .scoping").removeClass("grey");
            $(".fibre-journey .scoping .yellow_banner_text").text("Not Required").show(); 

            //Consent - Not Showing
            $(".fibre-journey .consent .yellow_banner_text").text("Not Required").show();
            $(".fibre-journey .consent").addClass("dark-pink");
            $(".fibre-journey .consent").removeClass("bright-pink");
    }

    if (sanitizedOrderHistory['complete']
        && sanitizedOrderHistory['live'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['live'].date);
        $(".fibre-journey .live").addClass("dark-pink");
        $(".fibre-journey .live").removeClass("grey");
        $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        $(".fibre-journey .live .date").text(orderHistoryDate);

        $(".fibre-journey .installation").addClass("dark-pink");
        $(".fibre-journey .installation").removeClass("grey");
        $(".fibre-journey .installation .yellow_banner_text").text("Complete").show();
        $(".fibre-journey .installation .date").text(orderHistoryDate);
    }

    if (sanitizedOrderHistory['complete']
        && sanitizedOrderHistory['scoping'].className) {
            $(".fibre-journey .scoping").addClass("dark-pink");
            $(".fibre-journey .scoping").removeClass("grey");
            $(".fibre-journey .scoping .yellow_banner_text").text("Not Required").show();
        }    

    if (sanitizedOrderHistory['complete']
        && sanitizedOrderHistory['consent'].className) {
            $(".fibre-journey .consent").addClass("dark-pink");
            $(".fibre-journey .consent").removeClass("grey");
            $(".fibre-journey .consent .yellow_banner_text").text("Not Required").show();
        }

    if (sanitizedOrderHistory['consent-granted']
        && sanitizedOrderHistory['consent-granted'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['consent-granted'].date);
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .order").removeClass("bright-pink");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");

        $(".fibre-journey .consent .yellow_banner_text").text("Consent Granted").show();
        $(".fibre-journey .consent .date").text(orderHistoryDate);

        $(".fibre-journey .installation").addClass("grey");
        $(".fibre-journey .installation").removeClass("dark-pink");

        $(".fibre-journey .live .yellow_banner_text").hide();
        $(".fibre-journey .live").addClass("grey");
        $(".fibre-journey .live").removeClass("dark-pink");
        $(".fibre-journey .live").removeClass("bright-pink");

        if (stage === "service-is-active") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        }

        if (stage === "complete") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        }
    }

    if (sanitizedOrderHistory['awaiting-response']
        && sanitizedOrderHistory['awaiting-response'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['awaiting-response'].date);
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");

        $(".fibre-journey .consent .yellow_banner_text").text("Awaiting Response").show();
        $(".fibre-journey .consent .date").text(orderHistoryDate);

        if (stage === "service-is-active") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        }

        if (stage === "complete") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        }
    }
    if (sanitizedOrderHistory['consent-required']
        && sanitizedOrderHistory['consent-required'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['consent-required'].date);
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("bright-pink");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");        
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");

        $(".fibre-journey .consent .yellow_banner_text").text("Consent Required").show();
        $(".fibre-journey .consent .date").text(orderHistoryDate);

        if (stage === "service-is-active") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        }

        if (stage === "complete") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        }
    }

    if (sanitizedOrderHistory['wait-for-quote-acceptance']
        && sanitizedOrderHistory['wait-for-quote-acceptance'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['wait-for-quote-acceptance'].date);
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .scoping .yellow_banner_text").text("Wait for quote acceptance").show();
        $(".fibre-journey .scoping .date").text(orderHistoryDate);

        if (stage === "service-is-active") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        }

        if (stage === "complete") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        }
    }
    if (sanitizedOrderHistory['generate-quote']
        && sanitizedOrderHistory['generate-quote'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['generate-quote'].date);
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");
        $(".fibre-journey .scoping .yellow_banner_text").text("Generate quote").show();
        $(".fibre-journey .scoping .date").text(orderHistoryDate);

        if (stage === "service-is-active") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        }

        if (stage === "complete") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        }
    }

    if (sanitizedOrderHistory['processing']
        && sanitizedOrderHistory['processing'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['processing'].date);
        $(".fibre-journey .order").addClass("bright-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .order .yellow_banner_text").text("Processing").show();
        $(".fibre-journey .order .date").text(orderHistoryDate);

        if (stage === "service-is-active") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Service is active").show();
        }

        if (stage === "complete") {
            $(".fibre-journey .installation").addClass("dark-pink");
            $(".fibre-journey .installation").removeClass("grey");

            $(".fibre-journey .live").addClass("bright-pink");
            $(".fibre-journey .live").removeClass("grey");
            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .live .date").text(orderDate);
            $(".fibre-journey .installation .date").hide();
            $(".fibre-journey .live .yellow_banner_text").text("Complete").show();
        }
    }

    if (sanitizedOrderHistory['order-created']
        && sanitizedOrderHistory['order-created'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
        $(".fibre-journey .order").addClass("bright-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .order .yellow_banner_text").text("Order Created").show();
        $(".fibre-journey .order .date").text(orderHistoryDate);

        if (stage === "consent-required" 
            || stage === "awaiting-response" 
            || stage === "consent-granted"
            || stage === "generate-quote"
            || stage === "complete"
            || stage === "service-is-active") {

            $(".fibre-journey .order").addClass("dark-pink");
            $(".fibre-journey .order").removeClass("bright-pink");

            var orderDate = formatDateTime(sanitizedOrderHistory['order-created'].date);
            $(".fibre-journey .order .date").text(orderDate);

        }

        

        if (sanitizedOrderHistory['consent-granted']
        && sanitizedOrderHistory['consent-granted'].className && sanitizedOrderHistory['generate-quote']
        && sanitizedOrderHistory['generate-quote'].className )   {
            console.log("test consent-granted");
            $(".fibre-journey .order").addClass("dark-pink");
            $(".fibre-journey .order").removeClass("bright-pink");
        }
    }

    //getting shipping DATE    
    if (sanitizedOrderHistory['item-has-shipped']
        && sanitizedOrderHistory['item-has-shipped'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['item-has-shipped'].date);
       // log(sanitizedOrderHistory['item-has-shipped'].date);
        $(".modem_tracking").text("Your Fibre Modem has shipped on " + sanitizedOrderHistory['item-has-shipped'].date).show();
    }
    if (sanitizedOrderHistory['complete']
        && sanitizedOrderHistory['complete'].className) {
        var orderHistoryDate = formatDateTime(sanitizedOrderHistory['complete'].date);
        log(sanitizedOrderHistory['complete'].date);
        $(".fibre-journey .order").addClass("dark-pink");
        $(".fibre-journey .order").removeClass("grey");
        $(".fibre-journey .scoping").addClass("dark-pink");
        $(".fibre-journey .scoping").removeClass("grey");        
        $(".fibre-journey .consent").addClass("dark-pink");
        $(".fibre-journey .consent").removeClass("grey");
        $(".fibre-journey .installation").addClass("dark-pink");
        $(".fibre-journey .installation").removeClass("grey");
        $(".fibre-journey .live").addClass("bright-pink");
        $(".fibre-journey .live .date").text(orderHistoryDate);
        $(".fibre-journey .live").removeClass("grey");        
        
        
    }    
};

//AJAX function
var fetchOrderStatus = function(id) {
    $.ajax({
        type:     'GET',
        //url:    '/ordertracker/' + id,
        // url:    'http://www.webimpact.co.nz/Apiordertracker/03_Weekend_try/db.json' + '/ordertracker/' + id,
        //url :     "http://www.webimpact.co.nz/Apiordertracker/03_Weekend_try/order.json",
        //url :     "/order.json",
        url: "/rest/v1/ordertracker?orderId=" + id,
        dataType: 'json',
        timeout:  20000,
        success: function(data, xhr, res) {
            console.log('Request Success: ' + res.status);
            console.log(data);
            fetchOrderDone(data);
        },
        error: function(res, type, message) {
            console.log('Request Error');
            console.log(res);
            console.log(type);

            if (type === 'timeout') {
                //console.log('timeout api error');
                $(".not_found_div").text("Sorry, an error has occurred, please try again later.").show();
            }
            var status = res.status;
            console.log(res.status);
            if (status === 404) {
                $(".not_found_div").text("Order not found").show();

            }
            if (status === 400) {
                $(".not_found_div").text("Invalid Input").show();
            }
            if (status === 500) {
                $(".not_found_div").text("Unexpected error occurred").show();
            }
        }
    });
};


//function for initializing 
var init = function() {

    var id = getOrderIdParam('orderId');
    console.log('order id: ' + id);

    if ( ! id) {
        return displayOrderInput();
    }

    showOrder(id);
    fetchOrderStatus(id);
};

//initialize document 
$(document).ready(function() {
    init();
});